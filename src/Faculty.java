import java.util.SplittableRandom;

public class Faculty extends Employee{
    private String [] course;
    /**
     *  constructor
     */
    public Faculty(String name, double salary,String []course){
        super(name,salary);
        this.course=course;

    }


    public String [] getCourse(){
        String [] course=new String[this.course.length];
        //making a copy
        for (int i=0;i<this.course.length;i++){
            course[i]=this.course[i];
        }
        return course;
    }
    public void setCourse(String []course){
        this.course=new String[course.length];
        for (int i=0; i<course.length;i++){
            this.course[i]=course[i];
        }
    }
    public  String getCourseNames(){
        String Names = "";
        for (int i = 0; i < this.course.length; i++){
            Names += this.course[i] + " ";
        }
        return Names;
    }
    public String toString(){
        String result = "The name of the professor is " + getName() + " and salary is " + getSalary() +
                "\nThe name of the courses is "+ "is " + getCourseNames();

        return result;
    }


}



