public class Employee implements Employeerules {
    private double salary;
    private String name;

    /**
     * Default Constructor.
     */
    public Employee (){
        this.salary=0.00;
        this.name=null;
    }
    //constructor
    public Employee(String name,double salary){
        this.name=name;
        this.salary=salary;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String res="The name of the employee is"+name+ "and the salary is $" +salary;
        return res;
    }
}
